<?php
 /* Template Name: Data Centraliztion */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$section_two = get_field('section_two');
$orange_container = get_field('orange_container');
?>


<div class="container mb-5">
  <div class="row">
    <div class="col">
      <h1 class="text-center mb-4 mt-4"><?php echo $section_one["heading"]; ?></h1>
      <p class="text-center"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>

<div class="container mb-4">
  <div class="row">
    <div class="col-12 col-xl-10 mx-xl-auto mb-5">
      <h3 class="text-center text-xl-left"><?php echo $section_two["heading_1"]; ?></h3>
      <p class="small-p text-center text-xl-left"><?php echo $section_two["body_1"]; ?></p>
    </div>
  </div>
  <div class="row">
    <div class="col12 col-xl-10 mx-xl-auto mb-5">
      <h3 class="text-center text-xl-left"><?php echo $section_two["heading_2"]; ?></h3>
      <p class="small-p text-center text-xl-left"><?php echo $section_two["body_2"]; ?>  </p>
    </div>
  </div>
  <div class="row">
    <div class="col12 col-xl-10 mx-xl-auto">
      <h3 class="text-center text-xl-left"><?php echo $section_two["heading_3"]; ?></h3>
      <p class="small-p text-center text-xl-left"><?php echo $section_two["body_3"]; ?></p>
      <p class="small-p text-center text-xl-left"><?php echo $section_two["body_3+"]; ?> </p>
    </div>
  </div>
</div>

<div class="container-fluid orange-container">
  <div class="container pt-5 pb-5">
    <div class="row">
      <div class="col">
        <h2 class="text-center white mb-4"><?php echo $orange_container["heading"]; ?></h2>
        <p class="text-center white mb-4"><?php echo $orange_container["body"]; ?></p>
        <a href="<?php echo $orange_container["link"]; ?>" class="blueBtn d-block mx-auto" style="	height: 34px;max-width: 131px;"><?php echo $orange_container["cta"]; ?></a>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
