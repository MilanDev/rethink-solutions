<?php
 /* Template Name: Learning Centre White Papers */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$learn_more_section = get_field('learn_more_section');
?>
<div class="white-papers">

<div class="container mb-5 mt-5">
  <div class="row">
    <div class="col-12">
      <h1 class="text-center"><?php echo $section_one["heading"]; ?></h1>
      <p class="text-center"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>

<div class="container mt-5 mb-4">
  <div class="row mt-2">
    <?php
  // Example argument that defines three posts per page.
  $args = array( 'posts_per_page' => 5 );

  // Variable to call WP_Query.
  $the_query = new WP_Query( $args );

  if ( $the_query->have_posts() ) :
      // Start the Loop
      while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <div class="col-12 col-md-6 col-lg-4">
            <div class="card mx-auto mb-5">
        <img class="img-fluid d-block" src="https://via.placeholder.com/376x284" alt="">
        <div class="card-body">
          <span><?php the_date(); ?><span class="author-span">/ by <?php the_author(); ?></span></span>
          <h5 class="card-title mt-2"><?php the_title(); ?></h5>
        </div>
      </div>
    </div>
    <?php  endwhile;
  else:
  // If no posts match this query, output this text.
      _e( 'Sorry, no posts matched your criteria.', 'textdomain' );
  endif;

  wp_reset_postdata();
  ?>

</div>
</div>

<div class="container-fluid" style="background-color:#F99828;">
  <div class="container">
    <div class="row">
      <div class="col pt-5 pb-5">
       <h2 class="text-center white mb-4" style="font-size:32px;"><?php echo $learn_more_section["heading"]; ?></h2>
       <a href="<?php echo $learn_more_section["link"]; ?>" class="blueBtn text-center mx-auto d-block" style="height: 34px;	width: 149px;"><?php echo $learn_more_section["cta"]; ?></a>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
