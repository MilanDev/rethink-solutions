<?php
 /* Template Name: About us Team */
get_header();
include('inc/inner-pages-hero.php');
$our_team = get_field('our_team');
$opportunities_section = get_field('opportunities_section');
?>


<div class="container">
    <div class="row">
      <div class="col-12">
        <p class="text-center mt-4 mb-4"><?php echo $our_team["body"]; ?></p>
      </div>
    </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-12  col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_1_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_1_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_1_title"]; ?></p>
    </div>
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_2_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_2_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_2_title"]; ?></p>
    </div>
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_3_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_3_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_3_title"]; ?></p>
    </div>
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_4_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_4_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_4_title"]; ?></p>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_5_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_5_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_5_title"]; ?></p>
    </div>
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_6_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_6_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_6_title"]; ?></p>
    </div>
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_7_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_7_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_7_title"]; ?></p>
    </div>
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_8_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_8_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_8_title"]; ?></p>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_9_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_9_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_9_title"]; ?></p>
    </div>
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_10_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_10_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_10_title"]; ?></p>
    </div>
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_11_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_11_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_11_title"]; ?></p>
    </div>
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $our_team["member_12_image"]; ?>" alt="">
      <p class="text-center mb-0"><?php echo $our_team["member_12_name"]; ?></p><p class="text-center"> <?php echo $our_team["member_12_title"]; ?></p>
    </div>
  </div>
</div>

<div class="container-fluid orange-container">
  <div class="row pt-5 pb-5">
    <div class="col text-center">
      <h3 class="text-center white"><?php echo $opportunities_section["heading"]; ?></h3>
      <a href="<?php echo $opportunities_section["link"]; ?>" class="blueBtn post-btn p-1" style="color:white;"><?php echo $opportunities_section["cta"]; ?></a>

    </div>
  </div>
</div>

<?php get_footer(); ?>
