

<?php get_header();
 /* Template Name: Home */
 $banner = get_field('banner');
 $client_logos = get_field('client_logos');
 $fact_points = get_field('fact_points');
 $main_body_section = get_field('main_body_section');
 $slider = get_field('slider');

?>
<div class="home">
<div class="container-fluid home-container" style="background-image:url(<?php the_field('hero_background'); ?>);">
<div class="container hero">
  <div class="row">
    <div class="col-sm-12 col-md-5">
      <h1 class="hero-h1 text-center text-md-left"><?php echo $banner["heading"]; ?></h1>
      <p class="hero-p text-center text-md-left"><?php echo $banner["body"]; ?></p>
      <a class="hero-button blue-button mx-auto mx-md-0 d-block no-underline pt-1" href="<?php echo $banner["link"]; ?>"><?php echo $banner["cta"]; ?></a>
    </div>
    <div class="col-sm-12 col-md-7">
      <img class="hero-img img-fluid" src="<?php echo $banner['hero_image']; ?>" />
    </div>
  </div>
</div>
</div>

<div class="container logos mt-5 mb-5">
  <div class="row">
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_1"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_2"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_3"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_4"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_5"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_6"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100 mt-5" src="<?php echo $client_logos["logo_7"] ?>" />
    </div>
  </div>
</div>

<div class="container-fluid blue-banner">
  <div class="container">
  <div class="row">
    <div class="col-12 text-center">
      <h3 class="blue-banner-h3"><?php echo $fact_points["heading"] ?></h3>
    </div>
  </div>
</div>
  <div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-4 banner-title text-center">
          <h2><?php echo $fact_points["column_1_heading"] ?></h2><br/>
          <p><?php echo $fact_points["column_1_body"] ?></p>
        </div>
        <div class="col-sm-12 col-md-4 banner-title text-center">
          <h2><?php echo $fact_points["column_2_heading"] ?></h2><br/>
          <p><?php echo $fact_points["column_2_body"] ?></p>
        </div>
        <div class="col-sm-12 col-md-4 banner-title text-center">
          <h2><?php echo $fact_points["column_3_heading"] ?></h2><br/>
          <p><?php echo $fact_points["column_3_body"] ?></p>
        </div>
      </div>
    </div>
  </div>

  <section class="left-right-containers">

<div class="container">
  <div class="row row-left">
    <div class="col-sm-12 text-center col-md-7 text-md-left">
      <h4 class="title"><?php echo $main_body_section['automation_small_heading']; ?></h4> <br />
      <h3 class="subtitle"><?php echo $main_body_section['automation_big_heading']; ?></h3>
      <p class="content"><?php echo $main_body_section['automation_body']; ?></p>
       <a class="blueBtn p-2" href="<?php echo $main_body_section['automation_link']; ?>"><?php echo $main_body_section['automation_cta']; ?></a>
    </div>
    <div class="col-sm-12 text-center col-md-5">
      <img class="hero-img img-fluid" src="<?php echo $main_body_section['automation_img']; ?>" />
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-12 text-center col-md-5">
      <img class="hero-img img-fluid mt-5" src="<?php echo $main_body_section['centralization_img']; ?>" />
    </div>
    <div class="col-sm-12 text-center col-md-7 text-md-right">
      <h4 class="title"><?php echo $main_body_section['centralization_small_heading']; ?></h4> <br />
      <h3 class="subtitle"><?php echo $main_body_section['centralization_big_heading']; ?></h3>
      <p class="content"><?php echo $main_body_section['centralization_body']; ?> </p>
       <a class="blueBtn p-2" href="<?php echo $main_body_section['centralization_link']; ?>"><?php echo $main_body_section['centralization_cta']; ?></a>
    </div>
  </div>
</div>

<div class="container">
  <div class="row row-left">
    <div class="col-sm-12 text-center col-md-8 text-md-left">
      <h4 class="title"><?php echo $main_body_section['itegrations_small_heading']; ?></h4> <br />
      <h3 class="subtitle"><?php echo $main_body_section['itegrations_big_heading']; ?></h3>
      <p class="content"><?php echo $main_body_section['itegrations_body']; ?></p>
       <a class="blueBtn explore-button p-2" href="<?php echo $main_body_section['itegrations_link']; ?>"><?php echo $main_body_section['itegrations_cta']; ?></a>
    </div>
    <div class="col-sm-12 text-center col-md-4">
      <img class="hero-img img-fluid" src="<?php echo $main_body_section['itegrations_img']; ?>" />
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-12 text-center col-md-5">
      <img class="hero-img img-fluid mt-5" src="<?php echo $main_body_section['support_img']; ?>" />
    </div>
    <div class="col-sm-12 text-center col-md-7 text-md-right">
      <h4 class="title"><?php echo $main_body_section['support_small_heading']; ?></h4> <br />
      <h3 class="subtitle"><?php echo $main_body_section['support_big_heading']; ?> </h3>
      <p class="content"><?php echo $main_body_section['support_body']; ?></p>
       <a class="blueBtn p-2" href="<?php echo $main_body_section['support_link']; ?>"><?php echo $main_body_section['support_cta']; ?></a>
    </div>
  </div>
</div>

</section>


<div class="container-fluid slider" style="background-image: url('<?php echo $slider['slider_background_image']; ?>');background-size: cover;background-repeat: no-repeat;height:auto;">
  <div class="container">
    <div class="row">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <h1 class="slider-h1"><?php echo $slider['slider_1_heading']; ?></h1>
            <p class="slider-testimonal"><?php echo $slider['slider_1_body']; ?></p>
            <div class="author-div d-flex">
              <div class="author-logo">
                <img src="<?php echo $slider['slider_1_author_img']; ?>" style="height: 85px;	width: 85px;" />
              </div>
            <p class="author-info ml-3 mt-auto"><?php echo $slider['slider_1_author']; ?></p>
            </div>
              <button class="blueBtn slider-button"><?php echo $slider['slider_1_link']; ?></button>
          </div>
          <div class="carousel-item">
            <h1 class="slider-h1"><?php echo $slider['slider_2_heading']; ?></h1>
            <p class="slider-testimonal"><?php echo $slider['slider_2_body']; ?></p>
            <div class="author-div d-flex">
              <div class="author-logo">
                <img src="<?php echo $slider['slider_2_author_img']; ?>" style="height: 85px;	width: 85px;" />
              </div>
            <p class="author-info ml-3 mt-auto"><?php echo $slider['slider_2_author']; ?></p>
            </div>
              <button class="blueBtn slider-button"><?php echo $slider['slider_2_link']; ?></button>
          </div>
          <div class="carousel-item">
            <h1 class="slider-h1"><?php echo $slider['slider_3_heading']; ?></b></h1>
            <p class="slider-testimonal"><?php echo $slider['slider_3_body']; ?></p>
            <div class="author-div d-flex">
              <div class="author-logo">
                <img src="<?php echo $slider['slider_3_author_img']; ?>" style="height: 85px;	width: 85px;" />
              </div>
            <p class="author-info ml-3 mt-auto"><?php echo $slider['slider_3_author']; ?></p>
            </div>
              <button class="blueBtn slider-button"><?php echo $slider['slider_3_link']; ?></button>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      </div>
    </div>
  </div>




  <div class="container blog-container mt-3 mb-5">
    <h2 class="blog-section-title text-center">Latest from the <b>Blog</b></h2>
    <div class="row">
      <div class="col-sm-12 col-md-5">
        <div class="first post-item mb-3 mb-md-0">
          <h2 class="post-heading-big">Titles for blog entries go over photos</h2>
          <p>sample blog text goes in this box too.</p>
          <button class="blueBtn post-btn" type="button" name="button">READ MORE</button>
        </div>

      </div>
      <div class="col">
        <div class="second post-item">
          <h2 class="post-heading-small">Titles for blog entries go over photos</h2>
          <p>sample blog text goes in this box too.</p>
          <button class="blueBtn post-btn" type="button" name="button">READ MORE</button>
        </div>
        <div class="third-f-holder d-flex">
          <div class="third post-item">
            <h2 class="post-heading-small">Titles for blog entries go over photos</h2>
            <p>sample blog text goes in this box too.</p>
            <button class="blueBtn post-btn" type="button" name="button">READ MORE</button>
          </div>
          <div class="fourth post-item">
            <h2 class="post-heading-small">Titles for blog entries go over photos</h2>
            <p>sample blog text goes in this box too.</p>
            <button class="blueBtn post-btn" type="button" name="button">READ MORE</button>
          </div>
        </div>
      </div>
    </div>
    <div class="row second-row">
      <div class="col d-md-flex">
      <div class="second-row-first-item post-item">
        <h2 class="post-heading-big">Titles for blog entries go over photos</h2>
        <p>sample blog text goes in this box too.</p>
        <button class="blueBtn post-btn" type="button" name="button">READ MORE</button>
      </div>
      <div class="second-row-second-item post-item mt-3 mt-md-0">
        <h2 class="post-heading-small">Titles for blog entries go over photos</h2>
        <p>sample blog text goes in this box too.</p>
        <button class="blueBtn post-btn" type="button" name="button">READ MORE</button>
      </div>
    </div>
    </div>
  </div>

</div>





<?php get_footer(); ?>
