<?php
 /* Template Name: Capture */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$section_two = get_field('section_two');
$section_three = get_field('section_three');
$section_four = get_field('section_four');
$orange_section = get_field('orange_section');
?>

<div class="capture">


<div class="container">
  <div class="row">
    <div class="col mt-5 mb-5">
      <h1 class="text-center"><?php echo $section_one["heading"]; ?></b></h1>
      <p class="text-center"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>

<div class="container pt-5 pb-5">
  <div class="row">
    <div class="col-12 col-lg-8">
      <h3 class="text-center text-lg-left"><?php echo $section_two["heading"]; ?></h3>
      <p class="text-center text-lg-left"><?php echo $section_two["body"]; ?></p>
    </div>
    <div class="col-12 col-lg-4">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $section_two["image"]; ?>" alt="">
    </div>
  </div>
</div>

<div class="container-fluid blue-section">
  <div class="container pt-5 pb-5">
    <div class="row">
      <div class="col-12 col-lg-4 order-2 order-lg-1">
            <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $section_three["image"]; ?>" alt="">
      </div>
        <div class="col-12 col-lg-8 order-1 order-lg-2">
          <h3 class="text-center text-lg-right mt-4"><?php echo $section_three["heading"]; ?></h3>
          <p class="text-center text-lg-right"><?php echo $section_three["body"]; ?></p>
        </div>
    </div>
  </div>
</div>

<div class="container pt-5 pb-5">
  <div class="row">
    <div class="col-12 col-lg-8">
      <h3 class="text-center text-lg-left"><?php echo $section_four["heading"]; ?></h3>
      <p class="text-center text-lg-left"><?php echo $section_four["body"]; ?></p>
    </div>
    <div class="col-12 col-lg-4">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $section_four["image"]; ?>" alt="">
    </div>
  </div>
</div>

<div class="container-fluid pt-5 pb-5 orange-section">
  <div class="row">
    <div class="col">
      <h3 class="text-center capture-in-action mb-3"><?php echo $orange_section["heading"]; ?></h3>
      <a href="<?php echo $orange_section["link"]; ?>" class="blueBtn d-block mx-auto w-100" style="	height: 34px;	max-width: 171px;"><?php echo $orange_section["cta"]; ?></a>
    </div>
  </div>
</div>

</div>
<?php get_footer(); ?>
