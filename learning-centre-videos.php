<?php
 /* Template Name: Learning Centre Videos */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$video_section = get_field('video_section');
$blog_section = get_field('blog_section');
?>

<div class="container mt-5 mb-3  mb-lg-5">
  <div class="row">
    <div class="col">
      <h1 class="text-center"><?php echo $section_one["heading"]; ?></h1>
      <p class="text-center"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>




<div class="container mb-4">
  <ul class="nav nav-justified nav-tabs mx-auto mb-5 mt-5" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="Tax-Management-tab" data-toggle="tab" href="#Tax-Management" role="tab" aria-controls="Tax-Management" aria-selected="true">Property Tax Management</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="itamlink-tab" data-toggle="tab" href="#itamlink" role="tab" aria-controls="itamlink" aria-selected="false">About itamlink</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="Testimonials-tab" data-toggle="tab" href="#Testimonials" role="tab" aria-controls="Testimonials" aria-selected="false">Testimonials</a>
    </li>
  </ul>
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="Tax-Management" role="tabpanel" aria-labelledby="Tax-Management-tab">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-6 pt-lg-5 mb-3">
            <h3 class="mb-3 text-center text-lg-left"><?php echo $video_section["heading"]; ?></h3>
            <p class="text-center text-lg-left"><?php echo $video_section["body"]; ?> </p>
          </div>
          <div class="col-12 col-lg-6">
            <video width="100%" height="285"  controls>
               <source src="<?php echo $video_section["video"]; ?>" type="video/mp4">
               <source src="<?php echo $video_section["video"]; ?>" type="video/ogg">
               Your browser does not support the video tag.
            </video>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="itamlink" role="tabpanel" aria-labelledby="itamlink-tab">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-6 pt-lg-5 mb-3">
            <h3 class="mb-3 text-center text-lg-left"><?php echo $video_section["heading"]; ?></h3>
            <p class="text-center text-lg-left"><?php echo $video_section["body"]; ?> </p>
          </div>
          <div class="col-12 col-lg-6">
            <video width="100%" height="285"  controls>
               <source src="<?php echo $video_section["video"]; ?>" type="video/mp4">
               <source src="<?php echo $video_section["video"]; ?>" type="video/ogg">
               Your browser does not support the video tag.
            </video>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="Testimonials" role="tabpanel" aria-labelledby="Testimonials-tab">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-6 pt-lg-5 mb-3">
            <h3 class="mb-3 text-center text-lg-left"><?php echo $video_section["heading"]; ?></h3>
            <p class="text-center text-lg-left"><?php echo $video_section["body"]; ?> </p>
          </div>
          <div class="col-12 col-lg-6">
            <video width="100%" height="285"  controls>
               <source src="<?php echo $video_section["video"]; ?>" type="video/mp4">
               <source src="<?php echo $video_section["video"]; ?>" type="video/ogg">
               Your browser does not support the video tag.
            </video>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row">
    <div class="col-12">
      <h2 class="text-center mt-5 mb-4">Rethink your property tax strategy, explore our <br class="d-none d-xl-block" /> resource links below to learn more. </h2>
    </div>
  </div>
</div>

<div class="container blog-container mt-5 mb-5">
  <div class="row">
    <div class="col-sm-12 col-md-5 pr-md-0">
      <div class="media-blog-box">
        <h2 class="post-heading-big">Property Tax White Papers</h2>
        <p></p>
        <a href="<?php echo $blog_section["link_1"]; ?>" class="blueBtn post-btn" style="color:white;"><?php echo $blog_section["cta_1"]; ?></a>
      </div>

    </div>
    <div class="col">
      <div class="media-news mt-3 mt-md-0">
        <h2 class="post-heading-small">Property Tax Blog</h2>
        <p class="m-0"></p>
        <a href="<?php echo $blog_section["link_2"]; ?>" class="blueBtn post-btn white" style="color:white;"><?php echo $blog_section["cta_2"]; ?></a>
      </div>
      <div class="media-releases m-0" style="margin-top:30px;">
        <h2 class="post-heading-small">Upcoming Webinars</h2>
        <p class="m-0"></p>
        <a href="<?php echo $blog_section["link_3"]; ?>" class="blueBtn post-btn" style="color:white;"><?php echo $blog_section["cta_3"]; ?></a>
      </div>
      </div>
    </div>
  </div>
</div>


<?php get_footer(); ?>
