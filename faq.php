<?php
 /* Template Name: FAQ */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$section_two = get_field('section_two');
$orange_container = get_field('orange_container');
?>



<div class="faq">

<div class="container mb-5">
  <div class="row">
    <div class="col">
      <h1 class="text-center mt-4 mb-4"><?php echo $section_one["heading"]; ?></h1>
      <p class="text-center"><?php echo $section_one["body"]; ?> </p>
    </div>
  </div>
</div>

<div class="container accordions mb-5">
  <div class="row">
    <div class="col">
        <div class="accordion" id="accordionExample">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h2 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="text-left"><?php echo $section_two["question_1_heading"]; ?><b></b></h4>
                </button>
              </h2>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body">
                <p><?php echo $section_two["question_2_answer"]; ?></p>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  <h4 class="text-left"><?php echo $section_two["question_2_heading"]; ?></h4>
                </button>
              </h2>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
              <div class="card-body">
                <p><?php echo $section_two["question_2_answer"]; ?></p>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  <h4 class="text-left"><?php echo $section_two["question_3_heading"]; ?></h4>
                </button>
              </h2>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
              <div class="card-body">
              <p><?php echo $section_two["question_3_answer"]; ?></p>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingFour">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                  <h4 class="text-left"><?php echo $section_two["question_4_heading"]; ?></h4>
                </button>
              </h2>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
              <div class="card-body">
              </p><?php echo $section_two["question_4_answer"]; ?></p>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingFive">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                  <h4 class="text-left"><?php echo $section_two["question_5_heading"]; ?></h4>
                </button>
              </h2>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
              <div class="card-body">
              <p><?php echo $section_two["question_5_answer"]; ?></p>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingSix">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                  <h4 class="text-left"><?php echo $section_two["question_6_heading"]; ?></h4>
                </button>
              </h2>
            </div>
            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
              <div class="card-body">
                <p><?php echo $section_two["question_6_answer"]; ?></p>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingSeven">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                  <h4 class="text-left"><?php echo $section_two["question_7_heading"]; ?></h4>
                </button>
              </h2>
            </div>
            <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
              <div class="card-body">
                <p><?php echo $section_two["question_7_answer"]; ?></p>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>

<div class="container-fluid orange-container">
  <div class="row pt-5 pb-5">
    <div class="col text-center">
      <h3 class="text-center white"><?php echo $orange_container["heading"]; ?></h3>
      <a href="<?php echo $orange_container["link"]; ?>" class="blueBtn post-btn p-1" style="color:white;"><?php echo $orange_container["cta"]; ?></a>
    </div>
  </div>
</div>
<?php get_footer(); ?>
