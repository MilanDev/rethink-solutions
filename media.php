<?php
 /* Template Name: About Us Media */
get_header();
include('inc/inner-pages-hero.php');
$first_section = get_field('first_section');
 $blog_section = get_field('blog_section');
?>


<div class="container-fluid media-first-row">
<div class="container">
  <div class="row">
    <div class="col-sm-12 col-md-7">
      <h2 class="mb-4 text-center text-md-left"><?php echo $first_section["heading"]; ?></h2>
      <p class="media-p text-center text-md-left"><?php echo $first_section["body"]; ?></p>
    </div>
    <div class="col-sm-12 col-md-5 text-center">
      <img class="img-fluid" src="<?php echo $first_section["image"]; ?>" />
    </div>
  </div>
</div>
</div>

<div class="container blog-container mt-5 mb-5">
  <div class="row">
    <div class="col-sm-12 col-md-5 pr-md-0">
      <div class="media-blog-box">
        <h2 class="post-heading-big">Videos</h2>
        <p></p>
        <a href="<?php echo $blog_section["link_1"]; ?>" class="blueBtn post-btn" style="color:white;"><?php echo $blog_section["cta_1"]; ?></a>
      </div>

    </div>
    <div class="col">
      <div class="media-news mt-3 mt-md-0">
        <h2 class="post-heading-small">In The News</h2>
        <p class="m-0"></p>
        <a href="<?php echo $blog_section["link_2"]; ?>" class="blueBtn post-btn" style="color:white;"><?php echo $blog_section["cta_2"]; ?></a>
      </div>
      <div class="media-releases m-0" style="margin-top:30px;">
        <h2 class="post-heading-small">Press Releases</h2>
        <p class="m-0"></p>
        <a href="<?php echo $blog_section["link_3"]; ?>" class="blueBtn post-btn" style="color:white;"><?php echo $blog_section["cta_3"]; ?></a>
      </div>
      </div>
    </div>
  </div>






<?php get_footer(); ?>
