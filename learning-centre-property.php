<?php
 /* Template Name: Learning Centre Property */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$section_two = get_field('section_two');
$section_three = get_field('section_three');
$section_four = get_field('section_four');
$section_five = get_field('section_five');
$blog_section = get_field('blog_section');
?>
<div class="property">


<div class="container first-container mb-3">
  <div class="row">
    <div class="col">
      <h2 class="text-center mt-5 mb-4"><?php echo $section_one["heading"]; ?></h2>
      <p class="text-center text-md-left"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>

<div class="container-fluid gray-container">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-9">
        <h2 class="mt-5 mb-4 text-center text-md-left"><?php echo $section_two["heading"]; ?> </h2>
        <p class="text-center text-md-left"><?php echo $section_two["body"]; ?> </p>
      </div>
      <div class="col-12 col-md-3">
        <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $section_two["image"]; ?>" alt="">
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-12">
      <h3 class="text-center mt-5 mb-5"><?php echo $section_three["big_heading"]; ?></h3>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-12 col-md-9">
      <h5 class="text-center text-md-left mb-3"><?php echo $section_three["small_heading"]; ?></h5>
      <p class="text-center text-md-left"><?php echo $section_three["body"]; ?></p>
      <a class="text-center text-md-left d-block" href="<?php echo $section_three["link"]; ?>" style="color: #00A9F4;	font-family: Roboto;	font-size: 16px;	font-weight: bold;	line-height: 19px;"><?php echo $section_three["cta"]; ?></a>
    </div>
    <div class="col-12 col-md-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $section_three["image"]; ?>" alt="">
    </div>
  </div>
</div>

<div class="container-fluid blue-container">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-3 order-2 order-md-1">
        <img class="mt-5 mb-5 img-fluid mx-auto d-block" src="<?php echo $section_four["image"]; ?>" alt="">
      </div>
      <div class="col-12 col-md-9 order-1 order-md-2">
        <h5 class="text-center text-md-right mt-5 mb-3"><?php echo $section_four["heading"]; ?></h5>
        <p class="text-center text-md-right"><?php echo $section_four["body"]; ?> </p>
        <a class="text-center text-md-right d-block" href="<?php echo $section_four["link"]; ?>" style="	"><?php echo $section_four["cta"]; ?></a>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-12 col-md-9">
      <h5 class="text-center text-md-left mt-5 mb-3"><?php echo $section_five["heading"]; ?></h5>
      <p class="text-center text-md-left"><?php echo $section_five["body"]; ?></p>
      <a class="text-center text-md-left d-block" href="<?php echo $section_five["link"]; ?>" style="font-family: Roboto;	font-size: 16px;	font-weight: bold;	line-height: 19px;"><?php echo $section_five["cta"]; ?></a>
    </div>
    <div class="col-12 col-md-3">
      <img class="mt-5 mb-5 img-fluid mx-auto d-block" src="https://via.placeholder.com/161x253" alt="">
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-12">
      <h4 class="text-center mt-5 mb-4">Rethink your property tax strategy, explore our <br class="d-none d-xl-block" /> resource links below to learn more. </h4>
    </div>
  </div>
</div>

<div class="container blog-container mt-5 mb-5">
  <div class="row">
    <div class="col-sm-12 col-md-5 pr-md-0">
      <div class="media-blog-box">
        <h2 class="post-heading-big">Property Tax White Papers</h2>
        <p></p>
        <a href="<?php echo $blog_section["link_1"]; ?>" class="blueBtn post-btn" style="color:white;"><?php echo $blog_section["cta_1"]; ?></a>
      </div>

    </div>
    <div class="col">
      <div class="media-news mt-3 mt-md-0">
        <h2 class="post-heading-small">Property Tax Blog</h2>
        <p class="m-0"></p>
        <a href="<?php echo $blog_section["link_2"]; ?>" class="blueBtn post-btn white" style="color:white;"><?php echo $blog_section["cta_2"]; ?></a>
      </div>
      <div class="media-releases m-0" style="margin-top:30px;">
        <h2 class="post-heading-small">Upcoming Webinars</h2>
        <p class="m-0"></p>
        <a href="<?php echo $blog_section["link_3"]; ?>" class="blueBtn post-btn" style="color:white;"><?php echo $blog_section["cta_3"]; ?></a>
      </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
