<?php
 /* Template Name: Our Customers */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$client_logos = get_field('client_logos');
$slider = get_field('slider');
$petsmart = get_field('petsmart');
$gallery_section = get_field('gallery_section');
?>
<div class="customers">


<div class="container customer-story-container">
  <div class="row">
    <div class="col-12">
      <h1 class="text-center mt-5"><?php echo $section_one["heading"]; ?></h1>
      <p class="mb-5 text-center"><?php echo $section_one["body_1"]; ?></p>
      <p class="text-center"><?php echo $section_one["body_2"]; ?></p>
    </div>
  </div>
</div>

<div class="container logos mt-5 mb-5">
  <div class="row">
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_1"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_2"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_3"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_4"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_5"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_6"] ?>" />
    </div>
    <div class="col-sm-6 col-md text-center">
      <img class="hero-img img-fluid w-100 mt-5" src="<?php echo $client_logos["logo_7"] ?>" />
    </div>
  </div>
</div>

<div class="container-fluid slider" style="background-image: url('<?php echo $slider['slider_background_image']; ?>');background-size: cover;background-repeat: no-repeat;height:auto;">
  <div class="container">
    <div class="row">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <h1 class="slider-h1"><?php echo $slider['slider_1_heading']; ?></h1>
            <p class="slider-testimonal"><?php echo $slider['slider_1_body']; ?></p>
            <div class="author-div d-flex">
              <div class="author-logo">
                <img src="<?php echo $slider['slider_1_author_img']; ?>" style="height: 85px;	width: 85px;" />
              </div>
            <p class="author-info ml-3 mt-auto"><?php echo $slider['slider_1_author']; ?></p>
            </div>
              <button class="blueBtn slider-button"><?php echo $slider['slider_1_link']; ?></button>
          </div>
          <div class="carousel-item">
            <h1 class="slider-h1"><?php echo $slider['slider_2_heading']; ?></h1>
            <p class="slider-testimonal"><?php echo $slider['slider_2_body']; ?></p>
            <div class="author-div d-flex">
              <div class="author-logo">
                <img src="<?php echo $slider['slider_2_author_img']; ?>" style="height: 85px;	width: 85px;" />
              </div>
            <p class="author-info ml-3 mt-auto"><?php echo $slider['slider_2_author']; ?></p>
            </div>
              <button class="blueBtn slider-button"><?php echo $slider['slider_2_link']; ?></button>
          </div>
          <div class="carousel-item">
            <h1 class="slider-h1"><?php echo $slider['slider_3_heading']; ?></b></h1>
            <p class="slider-testimonal"><?php echo $slider['slider_3_body']; ?></p>
            <div class="author-div d-flex">
              <div class="author-logo">
                <img src="<?php echo $slider['slider_3_author_img']; ?>" style="height: 85px;	width: 85px;" />
              </div>
            <p class="author-info ml-3 mt-auto"><?php echo $slider['slider_3_author']; ?></p>
            </div>
              <button class="blueBtn slider-button"><?php echo $slider['slider_3_link']; ?></button>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      </div>
    </div>
  </div>




<div class="container">
  <ul class="nav nav-justified nav-tabs mx-auto mb-5 mt-5" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="Petsmart-tab" data-toggle="tab" href="#Petsmart" role="tab" aria-controls="Petsmart" aria-selected="true">Petsmart</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="Duke-Realty-tab" data-toggle="tab" href="#Duke-Realty" role="tab" aria-controls="Duke-Realty" aria-selected="false">Duke Realty</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="Vistra-Energy-tab" data-toggle="tab" href="#Vistra-Energy" role="tab" aria-controls="Vistra-Energy" aria-selected="false">Vistra Energy</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="Forest-City-tab" data-toggle="tab" href="#Forest-City" role="tab" aria-controls="Forest-City" aria-selected="false">Forest City</a>
    </li>
  </ul>
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="Petsmart" role="tabpanel" aria-labelledby="Petsmart-tab">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-8">
            <h2 class="text-center text-lg-left"><?php echo $petsmart["petsmart_heading"]; ?></h2>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_1"]; ?></p>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_2"]; ?></p>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_3"]; ?></p>
          </div>
          <div class="col-12 col-lg-4">
              <?php echo do_shortcode( '[contact-form-7 id="130" title="White Papers form"]' ); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="Duke-Realty" role="tabpanel" aria-labelledby="Duke-Realty-tab">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-8">
            <h2 class="text-center text-lg-left"><?php echo $petsmart["petsmart_heading"]; ?></h2>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_1"]; ?></p>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_2"]; ?></p>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_3"]; ?></p>
          </div>
          <div class="col-12 col-lg-4">
              <?php echo do_shortcode( '[contact-form-7 id="130" title="White Papers form"]' ); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="Vistra-Energy" role="tabpanel" aria-labelledby="Vistra-Energy-tab">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-8">
            <h2 class="text-center text-lg-left"><?php echo $petsmart["petsmart_heading"]; ?></h2>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_1"]; ?></p>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_2"]; ?></p>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_3"]; ?></p>
          </div>
          <div class="col-12 col-lg-4">
              <?php echo do_shortcode( '[contact-form-7 id="130" title="White Papers form"]' ); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="Forest-City" role="tabpanel" aria-labelledby="Forest-City-tab">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-8">
            <h2 class="text-center text-lg-left"><?php echo $petsmart["petsmart_heading"]; ?></h2>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_1"]; ?></p>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_2"]; ?></p>
              <p class="text-center text-lg-left"><?php echo $petsmart["petsmart_body_3"]; ?></p>
          </div>
          <div class="col-12 col-lg-4">
              <?php echo do_shortcode( '[contact-form-7 id="130" title="White Papers form"]' ); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>






<div class="container sneak-peek-container pt-5">
  <div class="row">
    <div class="col-12">
      <h2 class="text-center"><?php echo $gallery_section["heading"]; ?></h2>
      <p class="text-center mb-5"><?php echo $gallery_section["body"]; ?></p>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-md-6 col-lg-4">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $gallery_section["image_1"]; ?>" alt="">
    </div>
    <div class="col-12 col-md-6 col-lg-4">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $gallery_section["image_2"]; ?>" alt="">
    </div>
    <div class="col-12 col-md-6 col-lg-4">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $gallery_section["image_3"]; ?>" alt="">
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-12 col-md-6 col-lg-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $gallery_section["image_4"]; ?>" alt="">
    </div>
    <div class="col-12 col-md-6 col-lg-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $gallery_section["image_5"]; ?>" alt="">
    </div>
    <div class="col-12 col-md-6 col-lg-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $gallery_section["image_6"]; ?>" alt="">
    </div>
    <div class="col-12 col-md-6 col-lg-3">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $gallery_section["image_7"]; ?>" alt="">
    </div>
  </div>
</div>

<div class="container-fluid sign-up-container">
<?php echo do_shortcode( '[contact-form-7 id="121" title="Sign Up"]' ); ?>
</div>
</div>

<?php get_footer(); ?>
