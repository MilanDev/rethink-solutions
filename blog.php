<?php
 /* Template Name: Blog */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
?>
<div class="blog">



<div class="container mt-2">
  <div class="row">
    <div class="col-12">
      <h1 class="text-center mt-5 mb-2"><?php echo $section_one["heading"]; ?></h1>
      <p class="text-center mb-5"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>


<div class="container mt-5 mb-4">
  <div class="row mt-2">
    <?php
  // Example argument that defines three posts per page.
  $args = array( 'posts_per_page' => 3 );

  // Variable to call WP_Query.
  $the_query = new WP_Query( $args );

  if ( $the_query->have_posts() ) :
      // Start the Loop
      while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <div class="col-12 col-md-6 col-lg-4">
            <div class="card mx-auto mb-5">
        <img class="img-fluid d-block" src="https://via.placeholder.com/376x284" alt="">
        <div class="card-body">
          <a href="#"><?php the_category(); ?></a>
          <span><?php the_date(); ?><span class="author-span">/ by <?php the_author(); ?></span></span>
          <h5 class="card-title mt-2"><?php the_title(); ?></h5>
          <p class="card-text"><?php the_content(); ?></p>
        </div>
      </div>
    </div>
    <?php  endwhile;
  else:
  // If no posts match this query, output this text.
      _e( 'Sorry, no posts matched your criteria.', 'textdomain' );
  endif;

  wp_reset_postdata();
  ?>

  </div>
</div>

<div class="container-fluid sign-up-container">
<?php echo do_shortcode( '[contact-form-7 id="121" title="Sign Up"]' ); ?>
</div>
</div>


</div>

<?php get_footer(); ?>
