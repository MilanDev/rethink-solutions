<?php
 /* Template Name: About Us Contact Us */
get_header();
include('inc/inner-pages-hero.php');
$first_section = get_field('first_section');
 ?>


<div class="container contact-us-container">
    <div class="col-12">
      <h1 class="mt-4 mb-4 text-center text-md-left"><?php echo $first_section["heading"]; ?></h1>
      <p class="text-center text-md-left"><?php echo $first_section["body_1"]; ?></p>
      <br />
        <p class="text-center text-md-left"><?php echo $first_section["body_2"]; ?><span class="info-span"><?php echo $first_section["email"]; ?></span></p>
           <br />
          <p class="mb-4 text-center text-md-left"><?php echo $first_section["body_3"]; ?>
          <span class="info-span"><?php echo $first_section["number"]; ?></span> directly. </p>
    </div>
  </div>

  <div class="container-fluid cf7-container">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-6">
          <?php echo do_shortcode( '[contact-form-7 id="118" title="Contact us"]' ); ?>
        </div>
        <div class="col-12 col-lg-6">
          <div class="text-center">
            <img src="<?php echo $first_section["cf7_image"]; ?>"/>
          </div>
        </div>
      </div>
    </div>
  </div>


<?php get_footer(); ?>
