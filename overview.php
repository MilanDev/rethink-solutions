<?php
 /* Template Name: Overview */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$problem_section = get_field('problem_section');
$solution_section = get_field('solution_section');
$features_section = get_field('features_section');
$orange_section = get_field('orange_section');
?>
<div class="overview">

<div class="container mt-5 mb-5">
  <div class="row">
    <div class="col">
        <h1 class="text-center"><?php echo $section_one["heading"]; ?></h1>
        <p class="text-center"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>

<div class="container mt-5 mb-5">
  <div class="row">
    <div class="col-12 col-lg-8 problem-solution">
      <h3 class="text-center text-lg-left"><?php echo $problem_section["heading"]; ?></h3>
      <p class="text-center text-lg-left">&#9679; <?php echo $problem_section["body_1"]; ?> </p>

        <p class="text-center text-lg-left">&#9679; <?php echo $problem_section["body_2"]; ?> </p>

        <p class="text-center text-lg-left">&#9679; <?php echo $problem_section["body_3"]; ?></p>

        <p class="text-center text-lg-left">&#9679; <?php echo $problem_section["body_4"]; ?></p>

        <p class="text-center text-lg-left">&#9679; <?php echo $problem_section["body_5"]; ?></p>
    </div>
    <div class="col-12 col-lg-4">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $problem_section["image"]; ?>" alt="">
    </div>
  </div>
</div>

<div class="container mb-5">
  <div class="row">
    <div class="col-12 col-lg-6 order-2 order-lg-1">
      <img class="mt-3 mb-4 img-fluid mx-auto mx-lg-0 mr-lg-auto d-block" src="<?php echo $solution_section["image"]; ?>" alt="">
    </div>
    <div class="col-12 col-lg-6 problem-solution order-1 order-lg-2">
      <h3 class="text-center text-lg-left"><?php echo $solution_section["heading"]; ?></h3>
        <p class="text-center text-lg-left">&#9679; <?php echo $solution_section["body_1"]; ?></p>

            <p class="text-center text-lg-left">&#9679; <?php echo $solution_section["body_2"]; ?></p>

            <p class="text-center text-lg-left"> &#9679;<?php echo $solution_section["body_3"]; ?></p>

          <p class="text-center text-lg-left">&#9679; <?php echo $solution_section["body_4"]; ?></p>

            <p class="text-center text-lg-left">&#9679; <?php echo $solution_section["body_5"]; ?></p>

            <p class="text-center text-lg-left">&#9679; <?php echo $solution_section["body_6"]; ?></p>

            <p class="text-center text-lg-left">&#9679; <?php echo $solution_section["body_7"]; ?></p>

            <p class="text-center text-lg-left">&#9679; <?php echo $solution_section["body_8"]; ?></p>
    </div>
  </div>
</div>

<div class="container-fluid features">
  <div class="container">
        <h2 class="text-center mb-5 pt-5 pb-5"><?php echo $features_section["heading"]; ?></h2>
    <div class="row">
      <div class="col-12 col-lg-6 mb-4">
          <div class="title d-flex">
          <img class="mt-3 mb-4 img-fluid d-block" src="<?php echo $features_section["image_1"]; ?>" alt="">
          <h4><?php echo $features_section["heading_1"]; ?></h4>
        </div>
        <p><?php echo $features_section["body_1"]; ?></p>
      </div>
      <div class="col-12 col-lg-6 mb-4">
        <div class="title d-flex">
        <img class="mt-3 mb-4 img-fluid d-block" src="<?php echo $features_section["image_3"]; ?>" alt="">
        <h4><?php echo $features_section["heading_3"]; ?></h4>
      </div>
      <p><?php echo $features_section["body_3"]; ?></p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-lg-6 mb-4">
        <div class="title d-flex">
        <img class="mt-3 mb-4 img-fluid d-block" src="<?php echo $features_section["image_3"]; ?>" alt="">
        <h4><?php echo $features_section["heading_3"]; ?></h4>
      </div>
      <p><?php echo $features_section["body_3"]; ?></p>
      </div>
      <div class="col-12 col-lg-6 mb-4">
        <div class="title d-flex">
        <img class="mt-3 mb-4 img-fluid d-block" src="<?php echo $features_section["image_4"]; ?>" alt="">
        <h4><?php echo $features_section["heading_4"]; ?></h4>
      </div>
      <p><?php echo $features_section["body_4"]; ?></p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-lg-6 mb-4">
        <div class="title d-flex">
        <img class="mt-3 mb-4 img-fluid d-block" src="<?php echo $features_section["image_5"]; ?>" alt="">
        <h4><?php echo $features_section["heading_5"]; ?></h4>
      </div>
      <p><?php echo $features_section["body_5"]; ?></p>
      </div>
      <div class="col-12 col-lg-6 mb-4">
        <div class="title d-flex">
        <img class="mt-3 mb-4 img-fluid d-block" src="<?php echo $features_section["image_6"]; ?>" alt="">
        <h4><?php echo $features_section["heading_6"]; ?></h4>
      </div>
      <p><?php echo $features_section["body_6"]; ?></p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-lg-6 mb-5">
        <div class="title d-flex">
        <img class="mt-3 mb-4 img-fluid d-block" src="<?php echo $features_section["image_7"]; ?>" alt="">
        <h4><?php echo $features_section["heading_7"]; ?></h4>
      </div>
      <p><?php echo $features_section["body_7"]; ?></p>
      </div>
      <div class="col-12 col-lg-6 mb-5">
        <div class="title d-flex">
        <img class="mt-3 mb-4 img-fluid d-block" src="<?php echo $features_section["image_8"]; ?>" alt="">
        <h4><?php echo $features_section["heading_8"]; ?></h4>
      </div>
      <p><?php echo $features_section["body_8"]; ?></p>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid pt-5 pb-5 contact-us">
  <div class="container">
    <div class="row">
      <div class="col">
        <h3 class="text-center"><?php echo $orange_section["heading"]; ?></h3><br />
        <p class="text-center"><?php echo $orange_section["body"]; ?></p> <br />
        <a href="<?php echo $orange_section["link"]; ?>" class="blueBtn d-block mx-auto" style="height: 34px;	max-width: 131px;"><?php echo $orange_section["cta"]; ?></a>
      </div>
    </div>
  </div>
</div>

</div>
<?php get_footer(); ?>
