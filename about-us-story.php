<?php
// Template Name: About us story
get_header();
include('inc/inner-pages-hero.php');
 $first_section = get_field('first_section');
 $client_logos = get_field('client_logos');
 $fact_points = get_field('fact_points');
 $blog_section = get_field('blog_section');
 ?>


 <div class="container about-us-story">
   <div class="row">
     <div class="col-12">
       <h1 class="mt-4 mb-4 text-center text-md-left"><?php echo $first_section["heading"]; ?></h1>
       <p class="text-center text-md-left"><?php echo $first_section["body_1"]; ?></p>
       <p class="mt-4 mb-4 text-center text-md-left"><?php echo $first_section["body_2"]; ?></p>
       <p class="text-center text-md-left"><?php echo $first_section["body_3"]; ?></p>
     </div>
   </div>
 </div>

   <div class="container logos mt-5 mb-5">
     <div class="row">
       <div class="col-sm-6 col-md text-center">
         <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_1"] ?>" />
       </div>
       <div class="col-sm-6 col-md text-center">
         <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_2"] ?>" />
       </div>
       <div class="col-sm-6 col-md text-center">
         <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_3"] ?>" />
       </div>
       <div class="col-sm-6 col-md text-center">
         <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_4"] ?>" />
       </div>
       <div class="col-sm-6 col-md text-center">
         <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_5"] ?>" />
       </div>
       <div class="col-sm-6 col-md text-center">
         <img class="hero-img img-fluid w-100" src="<?php echo $client_logos["logo_6"] ?>" />
       </div>
       <div class="col-sm-6 col-md text-center">
         <img class="hero-img img-fluid w-100 mt-5" src="<?php echo $client_logos["logo_7"] ?>" />
       </div>
     </div>
   </div>

   <div class="container-fluid blue-banner">
     <div class="container">
     <div class="row">
       <div class="col-12 text-center">
         <h3 class="blue-banner-h3"><?php echo $fact_points["heading"] ?></h3>
       </div>
     </div>
   </div>
     <div class="container">
       <div class="row">
           <div class="col-sm-12 col-md-4 banner-title text-center">
             <h2><?php echo $fact_points["column_1_heading"] ?></h2><br/>
             <p><?php echo $fact_points["column_1_body"] ?></p>
           </div>
           <div class="col-sm-12 col-md-4 banner-title text-center">
             <h2><?php echo $fact_points["column_2_heading"] ?></h2><br/>
             <p><?php echo $fact_points["column_2_body"] ?></p>
           </div>
           <div class="col-sm-12 col-md-4 banner-title text-center">
             <h2><?php echo $fact_points["column_3_heading"] ?></h2><br/>
             <p><?php echo $fact_points["column_3_body"] ?></p>
           </div>
         </div>
       </div>
     </div>

   <div class="container blog-container mt-5 mb-5">
     <div class="row">
       <div class="col-sm-12 col-md-5 pr-md-0">
         <div class="media-blog-box">
           <h2 class="media-h2">Media</h2>
           <p></p>
           <a href="<?php echo $blog_section["link_1"]; ?>" class="blueBtn post-btn" style="color:white;"><?php echo $blog_section["cta_1"]; ?></a>
         </div>

       </div>
       <div class="col">
         <div class="media-news mt-3 mt-md-0">
           <h2 class="post-heading-small">Career Opportunities</h2>
           <p class="m-0"></p>
           <a href="<?php echo $blog_section["link_2"]; ?>" class="blueBtn post-btn" style="color:white;"><?php echo $blog_section["cta_2"]; ?></a>
         </div>
         <div class="media-releases m-0" style="margin-top:30px;">
           <h2 class="post-heading-small">Contact Us</h2>
           <p class="m-0"></p>
           <a href="<?php echo $blog_section["link_3"]; ?>" class="blueBtn post-btn" style="color:white;"><?php echo $blog_section["cta_3"]; ?></a>
         </div>
         </div>
       </div>
     </div>

 <?php get_footer(); ?>
