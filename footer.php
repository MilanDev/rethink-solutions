<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<!-- Footer -->
	<footer class="page-footer font-small indigo">

	<!-- Footer Links -->
	<div class="container text-center text-md-left p-md-0">

	<!-- Grid row -->
	<div class="row">

	<!-- Grid column -->
	<div class="col-sm-6 col-md-2 mx-auto">

			<ul class="list-unstyled">
				<li>
					<a class="list-title" href="<?php echo site_url(''); ?>">Property Tax Software</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/Overview'); ?>">Itamlink Overview</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/customer-support'); ?>">Why Itamlink?</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/automation'); ?>">Automation</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/integrations'); ?>">Integrations</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/data-centralization'); ?>">Data Centralization</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/capture'); ?>">Capture</a>
				</li>
			</ul>

	</div>
	<!-- Grid column -->



	<!-- Grid column -->
	<div class="col-sm-6 col-md-2 mx-auto">

			<ul class="list-unstyled">
				<li>
					<a class="list-title" href="<?php echo site_url('/our-services'); ?>">Our Services</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/implementations'); ?>">Implementations</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/customer-experience'); ?>">Customer Experience</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/data-manager'); ?>">Data Management</a>
				</li>
			</ul>

	</div>
	<!-- Grid column -->



	<!-- Grid column -->
	<div class="col-sm-6 col-md-2 mx-auto">


			<ul class="list-unstyled">
				<li>
					<a class="list-title" href="<?php echo site_url('/our-customers'); ?>">Our Customers</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/our-customers'); ?>">Our Customers</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/testimonials'); ?>">Testimonials</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/working-together'); ?>">Working Together</a>
				</li>
			</ul>

	</div>
	<!-- Grid column -->



	<!-- Grid column -->
	<div class="col-sm-6 col-md-2 mx-auto">


			<ul class="list-unstyled">
				<li>
					<a class="list-title" href="<?php echo site_url('/learning-centre'); ?>">Learning Centre</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/property-tax-management'); ?>">Property Tax Management</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/white-papers'); ?>">White Papers</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/webinars'); ?>">Webinars</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/videos'); ?>">Videos</a>
				</li>
				<li>
					<a class="list-item" href="<?php echo site_url('/blog'); ?>">Blog</a>
				</li>
			</ul>

	</div>
	<!-- Grid column -->

	<!-- Grid column -->
	<div class="col-sm-6 col-md-2 mx-auto">

			<ul class="list-unstyled">
			 <li>
				 <a class="list-title" href="<?php echo site_url('/about-us'); ?>">About us</a>
			 </li>
			 <li>
				 <a class="list-item" href="<?php echo site_url('/our-story'); ?>">Our Story</a>
			 </li>
			 <li>
				 <a class="list-item" href="<?php echo site_url('/our-team'); ?>">Our Team</a>
			 </li>
			 <li>
				 <a class="list-item" href="<?php echo site_url('/career'); ?>">Career</a>
			 </li>
			 <li>
				 <a class="list-item" href="<?php echo site_url('/media'); ?>">Media</a>
			 </li>
			 <li>
				 <a class="list-item" href="<?php echo site_url('/faq'); ?>">FAQ</a>
			 </li>
			 <li>
				 <a class="list-item" href="<?php echo site_url('/news-events'); ?>">News & Events</a>
			 </li>
			 <li>
				 <a class="list-item" href="<?php echo site_url('/contact-us'); ?>">Contact Us</a>
			 </li>
			</ul>

	</div>
	<!-- Grid column -->

	<!-- Grid column -->
	<div class="col-sm-6 col-md-2 mx-auto">


	<ul class="list-unstyled">
	<li>
		<a class="list-title" href="<?php echo site_url('/'); ?>">Head Office</a>
	</li>
	<li>
		<a class="list-item" href="<?php echo site_url('/'); ?>">5 Kodiak Crescent,</a>
	</li>
	<li>
		<a class="list-item" href="<?php echo site_url('/'); ?>">Unit #10</a>
	</li>
	<li>
		<a class="list-title" href="<?php echo site_url('/'); ?>">Toronto, Ontario</a>
	</li>
	<li>
		<a class="list-item" href="<?php echo site_url('/'); ?>">416.224.8354</a>
	</li>
	<li>
		<a class="list-item" href="<?php echo site_url('/'); ?>">1,877.454.(ITAM) 4836</a>
	</li>
	</ul>

	</div>
	<!-- Grid column -->

	</div>
	<!-- Grid row -->

	</div>
	<!-- Footer Links -->

	<!-- logo and social media icons -->
	<div class="container p-md-0">
		<div class="row">
			<div class="col">
				<img class="img-fluid text-left" src="/rethinksolutions/wp-content/uploads/2019/09/footer-logo.png" />
			</div>
			<div class="col mt-auto">
				<div class="social-icons text-right">
					<a href="#"><img class="img-fluid ml-auto" src="/rethinksolutions/wp-content/uploads/2019/09/facebook.svg" /></a>
					<a href="#"><img class="img-fluid ml-auto" src="/rethinksolutions/wp-content/uploads/2019/09/linked-in.svg" /></a>
					<a href="#"><img class="img-fluid ml-auto" src="/rethinksolutions/wp-content/uploads/2019/09/twitter.svg" /></a>
				</div>
		</div>
	</div>
	<!-- logo and social media icons -->

	</footer>
	<!-- Footer -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>
