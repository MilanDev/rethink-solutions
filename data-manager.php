<?php
 /* Template Name: Data Manager */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$section_two = get_field('section_two');
$orange_container = get_field('orange_container');
?>


<div class="container mb-5">
  <div class="row">
    <div class="col">
      <h1 class="text-center mt-4 mb-4"><?php echo $section_one["heading"]; ?></h1>
      <p class="text-center"><?php echo $section_one["body"]; ?> </p>
    </div>
  </div>
</div>

<div class="container mb-5">
  <div class="row">
    <div class="col-12 col-lg-10 mx-lg-auto">
      <h3 class="text-center text-lg-left"><?php echo $section_two["heading"]; ?></h3>
      <p class="small-p text-center text-lg-left"><?php echo $section_two["body_1"]; ?></p>
      <p class="small-p text-center text-lg-left"><?php echo $section_two["body_2"]; ?></p>
    </div>
  </div>
</div>

<div class="container-fluid orange-container">
  <div class="container">
    <div class="row">
      <div class="col pt-5 pb-5">
        <h2 class="text-center mb-5 white"><?php echo $orange_container["heading"]; ?></h2>
        <a href="<?php echo $orange_container["link"]; ?>" class="blueBtn mx-auto d-block" style="	height: 34px;	max-width: 131px;"><?php echo $orange_container["cta"]; ?></a>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
