<?php
 /* Template Name: About Us Careers */
get_header();
include('inc/inner-pages-hero.php');
$first_section = get_field('first_section');
$second_section = get_field('second_section');
 ?>



  <div class="container buttons-container">
    <div class="row">
      <div class="col-12 text-center">
        <h1 class="mb-4"><?php echo $first_section["heading"]; ?></h1>
          <a style="color:#ffffff;" class="mr-2 blue-button-careers p-2" href="<?php echo $first_section["link_1"]; ?>"><?php echo $first_section["cta_1"]; ?></a>
          <a style="color: #F99828;" class="ml-2 yellow-button-careers p-2" href="<?php echo $first_section["link_2"]; ?>"><?php echo $first_section["cta_2"]; ?></a>
      </div>
    </div>
  </div>


  <div class="container-fluid careers-container">
    <div class="container">
      <div class="row mb-4">
        <div class="col-12 col-md-12 col-lg-4 mt-5 mt-lg-0">
          <img src="<?php echo $second_section["icon_1"]; ?>"/>
          <h4 class="mt-3"><?php echo $second_section["heading_1"]; ?></h4>
          <p class="mb-0"><?php echo $second_section["body_1"]; ?></p>
        </div>
        <div class="col-12 col-md-12 col-lg-4 mt-5 mt-lg-0">
          <img src="<?php echo $second_section["icon_2"]; ?>"/>
          <h4 class="mt-3"><?php echo $second_section["heading_2"]; ?></h4>
          <p class="mb-0"><?php echo $second_section["body_2"]; ?></p>
        </div>
        <div class="col-12 col-md-12 col-lg-4 mt-5 mt-lg-0">
          <img src="<?php echo $second_section["icon_3"]; ?>"/>
          <h4 class="mt-3"><?php echo $second_section["heading_3"]; ?></h4>
          <p class="mb-0"><?php echo $second_section["body_3"]; ?></p>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-12 col-lg-4 mt-5 mt-lg-0">
          <img src="<?php echo $second_section["icon_4"]; ?>"/>
          <h4 class="mt-3"><?php echo $second_section["heading_4"]; ?></h4>
          <p class="mb-0"><?php echo $second_section["body_4"]; ?> </p>
        </div>
        <div class="col-12 col-md-12 col-lg-4 mt-5 mt-lg-0">
          <img src="<?php echo $second_section["icon_5"]; ?>"/>
          <h4 class="mt-3"><?php echo $second_section["heading_5"]; ?></h4>
          <p class="mb-0"><?php echo $second_section["body_5"]; ?></p>
        </div>
        <div class="col-12 col-md-12 col-lg-4 mt-5 mt-lg-0">
          <img src="<?php echo $second_section["icon_6"]; ?>"/>
          <h4 class="mt-3"><?php echo $second_section["heading_6"]; ?></h4>
          <p class="mb-0"><?php echo $second_section["body_6"]; ?></p>
        </div>
      </div>
    </div>
  </div>

<?php get_footer(); ?>
