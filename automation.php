<?php
 /* Template Name: Automation */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$section_two = get_field('section_two');
$orange_container = get_field('orange_container');
?>

<div class="automation">


<div class="container">
  <div class="row">
    <div class="col">
      <h1 class="text-center mt-4 mb-4"><?php echo $section_one["heading"]; ?></h1>
      <h3 class="text-center mb-4"><?php echo $section_one["subheading"]; ?></h3>
      <p class="text-center hero-p"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>

<div class="container main-container pt-5 pb-5">
  <div class="row mt-4">
    <div class="col-12 col-lg-6 pr-lg-5">
      <h3 class="text-center text-lg-left"><?php echo $section_two["heading_1"]; ?></h3>
      <p class="text-center text-lg-left"><?php echo $section_two["body_1"]; ?></p>
    </div>
    <div class="col-12 col-lg-6 pr-lg-5">
      <h3 class="text-center text-lg-left"><?php echo $section_two["heading_2"]; ?></h3>
      <p class="text-center text-lg-left"><?php echo $section_two["body_2"]; ?> </p>
    </div>
  </div>
  <div class="row mb-4 mt-5">
    <div class="col-12 col-lg-6 pr-lg-5">
      <h3 class="text-center text-lg-left"><?php echo $section_two["heading_3"]; ?></h3>
      <p class="text-center text-lg-left"><?php echo $section_two["body_3"]; ?></p>
      <p class="text-center text-lg-left"><?php echo $section_two["body_3+"]; ?></p>
    </div>
    <div class="col-12 col-lg-6 pr-lg-5">
      <h3 class="text-center text-lg-left"><?php echo $section_two["heading_4"]; ?></h3>
      <p class="text-center text-lg-left"><?php echo $section_two["body_4"]; ?></p>
    </div>
  </div>
</div>

<div class="container-fluid orange-container">
  <div class="container pt-5 pb-5">
    <div class="row">
      <div class="col">
        <h2 class="text-center white"><?php echo $orange_container["heading"]; ?></h2>
            <p class="text-center white mt-3 mb-4"><?php echo $orange_container["body"]; ?></p>
            <a href="<?php echo $orange_container["link"]; ?>" class="blueBtn d-block mx-auto" style="	height: 34px;	max-width: 131px;"><?php echo $orange_container["cta"]; ?></a>
      </div>
    </div>
  </div>
</div>


</div>
<?php get_footer(); ?>
