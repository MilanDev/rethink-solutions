<?php
 /* Template Name: Customer Experience */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$section_two = get_field('section_two');
$orange_section = get_field('orange_section'); 
?>


<div class="container mb-5">
  <div class="row">
    <div class="col">
      <h1 class="text-center mt-4 mb-4"><?php echo $section_one["heading"]; ?></h1>
      <p class="text-center"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>

<div class="container">
  <div class="row mb-5">
    <div class="col-12 col-xl-10 mx-xl-auto">
      <h3 class="text-center text-xl-left"><?php echo $section_two["heading_1"]; ?> </h3>
      <p class="text-center text-xl-left small-p"><?php echo $section_two["body_1"]; ?> </p>
      <p class="text-center text-xl-left small-p"><?php echo $section_two["body_1+"]; ?> </p>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-12 col-xl-10 mx-xl-auto">
      <h3 class="text-center text-xl-left"><?php echo $section_two["heading_2"]; ?></h3>
      <p class="text-center text-xl-left small-p"><?php echo $section_two["body_2"]; ?></p>
    </div>
  </div>
</div>

<div class="container-fluid orange-container">
  <div class="container pt-5">
    <div class="row">
      <div class="col">
        <h2 class="text-center white form-h2">Contact Our Team to Learn More About Our<br class="d-none d-xl-block" />
          Philosophy on Customer Support
          </h2>
          <a href="<?php echo $orange_section["link"]; ?>" class="blueBtn d-block mx-auto w-100 mt-3 mb-3" style="	height: 34px;	max-width: 171px;"><?php echo $orange_section["cta"]; ?></a>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
