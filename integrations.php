<?php
 /* Template Name: Integrations */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$section_two = get_field('section_two');
$section_three = get_field('section_three');
$section_four = get_field('section_four');
$section_five = get_field('section_five');
?>
<div class="integrations">

<div class="container">
  <div class="row mb-5 mt-5">
    <div class="col">
      <h1 class="text-center mb-4"><?php echo $section_one["heading"]; ?></h1>
      <p class="text-center"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>

<div class="container">
  <div class="row pb-5 pt-5">
    <div class="col">
      <h2 class="text-center text-lg-left"><?php echo $section_two["heading"]; ?></h2>
      <p class="small-p text-center text-lg-left"><?php echo $section_two["body"]; ?></p>
    </div>
  </div>
  <div class="row pb-5 pt-5">
    <div class="col">
      <h2 class="text-center text-lg-left"><?php echo $section_three["heading"]; ?></h2>
      <img class="img-fluid d-block mt-5" src="<?php echo $section_three["image"]; ?>" alt="">
    </div>
  </div>
  <div class="row pb-5 pt-5">
    <div class="col">
      <h2 class="text-center text-lg-left"><?php echo $section_four["heading"]; ?></h2>
      <p class="small-p text-center text-lg-left"><?php echo $section_four["body"]; ?></p>
    </div>
  </div>
  <div class="row pb-5 pt-5">
    <div class="col">
      <h2 class="text-center text-lg-left"><?php echo $section_five["heading"]; ?></h2>
      <p class="small-p text-center text-lg-left"><?php echo $section_five["body"]; ?></p>
          <ul class="p-0" style="list-style:none;">
            <?php echo $section_five["list"]; ?>
        </ul>
    </div>
  </div>
</div>

<div class="container-fluid" style="background-color:#F99828;">
  <div class="row pt-5 pb-5">
      <div class="col">
        <h2 class="text-center white mb-5">Contact us to Become an Offical Integration Partner</h2>
      <?php echo do_shortcode( '[contact-form-7 id="135" title="Integrations"]' ); ?>
    </div>
  </div>
</div>

</div>
<?php get_footer(); ?>
