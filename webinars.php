<?php
// Template Name: Webinars
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$section_two = get_field('section_two');
 ?>
<div class="webinars">


<div class="container">
  <div class="row">
    <div class="col-12">
      <h1 class="text-center mt-5 mb-3"><?php echo $section_one["heading"]; ?></h1>
      <p class="text-center mb-5"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-12 col-lg-6">
      <h3 class="mb-0 text-center text-lg-left"><?php echo $section_two["heading"]; ?></h3>
      <h3 class="text-center text-lg-left"><?php echo $section_two["sub_heading"]; ?></h3>
      <p class="text-center text-lg-left"><?php echo $section_two["body_1"]; ?></p>
         <p class="text-center text-lg-left"><?php echo $section_two["body_2"]; ?></p>
    </div>
    <div class="col-12 col-lg-6">
      <?php echo do_shortcode( '[contact-form-7 id="122" title="Webinar Sign Up"]' ); ?>
    </div>
  </div>
</div>
</div>
<?php get_footer(); ?>
