<?php
 /* Template Name: Why Itamlink */
get_header();
include('inc/inner-pages-hero.php');
$section_one = get_field('section_one');
$section_two = get_field('section_two');
$section_three = get_field('section_three');
$blue_container = get_field('blue_container');
$section_four = get_field('section_four');
$slider = get_field('slider');
$orange_container = get_field('orange_container');
?>
<div class="why-itamlink">


<div class="container mt-5 mb-5">
  <div class="row">
    <div class="col">
      <h1 class="text-center text-lg-left"><?php echo $section_one["heading"]; ?></b></h1>
      <p class="text-center text-lg-left"><?php echo $section_one["body"]; ?></p>
    </div>
  </div>
</div>


<div class="container-fluid light-blue-container">
  <div class="container pt-5 pb-5">
    <div class="row">
      <div class="col">
        <h2 class="text-center text-lg-right"><?php echo $section_two["heading"]; ?></h2>
        <p class="text-center text-lg-right"><?php echo $section_two["body"]; ?></p>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row mt-5 mb-5">
    <div class="col-12 col-lg-6">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $section_three["image_1"]; ?>" alt="">
      <h3 class="text-center text-lg-left"><?php echo $section_three["heading_1"]; ?></h3>
      <p class="text-center text-lg-left small-p"><?php echo $section_three["body_1"]; ?></p>
    </div>
    <div class="col-12 col-lg-6">
      <img class="mt-3 mb-4 img-fluid mx-auto d-block" src="<?php echo $section_three["image_2"]; ?>" alt="">
      <h3 class="text-center text-lg-left"><?php echo $section_three["heading_2"]; ?></h3>
      <p class="text-center text-lg-left small-p"><?php echo $section_three["body_2"]; ?></p>
    </div>
  </div>
</div>

<div class="container-fluid blue-container">
  <div class="row pt-5 pb-5">
    <div class="col">
      <h2 class="text-center white"><?php echo $blue_container["heading"]; ?></h2>
      <a href="<?php echo $blue_container["link"]; ?>" class="text-center d-block mx-auto mt-5 pt-1 orange-btn"><?php echo $blue_container["cta"]; ?></a>
    </div>
  </div>
</div>

<div class="container">
  <div class="row mt-5 mb-5">
    <div class="col">
      <h4 class="text-center text-lg-left"><?php echo $section_four["heading_1"]; ?></h4>
      <p class="text-center text-lg-left small-p"><?php echo $section_four["body_1"]; ?> </p>
      <a href="<?php echo $section_four["link_1"]; ?>" class="text-center text-lg-left d-block"><?php echo $section_four["cta_1"]; ?></a>
    </div>
  </div>
  <div class="row mt-5 mb-5">
    <div class="col">
      <h4 class="text-center text-lg-right"><?php echo $section_four["heading_2"]; ?></h4>
      <p class="text-center text-lg-right small-p"><?php echo $section_four["body_2"]; ?>  </p>
      <a href="<?php echo $section_four["link_2"]; ?>" class="text-center text-lg-right d-block"><?php echo $section_four["cta_2"]; ?></a>
    </div>
  </div>
  <div class="row mt-5 mb-5">
    <div class="col">
      <h4 class="text-center text-lg-left"><?php echo $section_four["heading_3"]; ?></h4>
      <p class="text-center text-lg-left small-p"><?php echo $section_four["body_3"]; ?> </p>
      <a href="<?php echo $section_four["link_3"]; ?>" class="text-center text-lg-left d-block"><?php echo $section_four["cta_3"]; ?></a>
    </div>
  </div>
</div>

<div class="container-fluid slider" style="background-image: url('<?php echo $slider['slider_background_image']; ?>');background-size: cover;background-repeat: no-repeat;height:auto;">
  <div class="container">
    <div class="row">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <h1 class="slider-h1"><?php echo $slider['slider_1_heading']; ?></h1>
            <p class="slider-testimonal"><?php echo $slider['slider_1_body']; ?></p>
            <div class="author-div d-flex">
              <div class="author-logo">
                <img src="<?php echo $slider['slider_1_author_img']; ?>" style="height: 85px;	width: 85px;" />
              </div>
            <p class="author-info ml-3 mt-auto"><?php echo $slider['slider_1_author']; ?></p>
            </div>
              <button class="blueBtn slider-button"><?php echo $slider['slider_1_link']; ?></button>
          </div>
          <div class="carousel-item">
            <h1 class="slider-h1"><?php echo $slider['slider_2_heading']; ?></h1>
            <p class="slider-testimonal"><?php echo $slider['slider_2_body']; ?></p>
            <div class="author-div d-flex">
              <div class="author-logo">
                <img src="<?php echo $slider['slider_2_author_img']; ?>" style="height: 85px;	width: 85px;" />
              </div>
            <p class="author-info ml-3 mt-auto"><?php echo $slider['slider_2_author']; ?></p>
            </div>
              <button class="blueBtn slider-button"><?php echo $slider['slider_2_link']; ?></button>
          </div>
          <div class="carousel-item">
            <h1 class="slider-h1"><?php echo $slider['slider_3_heading']; ?></b></h1>
            <p class="slider-testimonal"><?php echo $slider['slider_3_body']; ?></p>
            <div class="author-div d-flex">
              <div class="author-logo">
                <img src="<?php echo $slider['slider_3_author_img']; ?>" style="height: 85px;	width: 85px;" />
              </div>
            <p class="author-info ml-3 mt-auto"><?php echo $slider['slider_3_author']; ?></p>
            </div>
              <button class="blueBtn slider-button"><?php echo $slider['slider_3_link']; ?></button>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      </div>
    </div>
  </div>

  <div class="container-fluid orange-container">
    <div class="row pt-5 pb-5">
      <div class="col">
        <h2 class="text-center white"><?php echo $orange_container["heading"]; ?></h2>
        <p class="text-center white mt-4 mb-4"><?php echo $orange_container["body"]; ?></p>
        <a href="<?php echo $orange_container["link"]; ?>" class="blueBtn d-block text-center mx-auto" style="	height: 34px;	max-width: 131px;"><?php echo $orange_container["cta"]; ?></a>
      </div>
    </div>
  </div>

</div>


<?php get_footer(); ?>
